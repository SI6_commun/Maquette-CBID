<!DOCTYPE HTML>
<meta charset="UTF-8">
<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css">
		<link rel="stylesheet" href="style_menu.css" type="text/css">
		<link rel="stylesheet" href="style_form.css" type="text/css">
		<link rel="stylesheet" href="knacss.css">
	</head>
	<body>
		<?php include_once('header.html')?>

	<div class="titre">
		Ajout d'une entrée budégtaire
	</div>

	<form id="formulaire">
		<p>Veuillez entrée :</p>

		<label for="date">Une date :</label>
		<input type="date" name="date" value="2000-01-01" required>
		<br>

		<label for="date_MES">Une date de mise en service :</label>
		<input type="date" name="date_MES" value="2000-01-01" required>
		<br>

		<div>
			<label for="statut">Le statut de l'entrée budgétaire :</label>
			<br>
			<label for="prevue">Prévue : </label> <input id="prevue" type="radio" name="statut" value="prevue" checked>
			<label for="engagee">Engagée :</label> <input id="engagee" type="radio" name="statut" value="engagee">
			<label for="pointee">Pointée :</label> <input id="pointee" type="radio" name="statut" value="pointee">
		</div>
		<br>

		<label for="prestataire">Un prestataire :</label>
		<input type="text" name="prestataire" placeholder="prestataire" required>
		<br>

		<label for="commentaire">Un commentaire :</label><br>
		<textarea placeholder="Exemple ..." name="commentaire" cols="30" rows="6"></textarea>
		<br>
		<br>

		<label for="responsable">Un responsable :</label>
		<input type="text" name="responsable" placeholder="responsable" required>
		<br>

		<label for="ligne_associe">Un N° de ligne associée à une entrée :</label>
		<input type="number" name="ligne_associe" value="1" min="1" required>
		<br>

		<label for="ligne_associe">Un montant d'écriture :</label>
		<input type="number" name="ligne_associe" value="1" min="1" required>
		<br>

		<div>
			<label for="paiement">Un moyen de paiement :</label>
			<br>
			<label>
				<input type="radio" name="paiement" id="visa" checked />
				<img src="img/visa.png" alt="visa"/>
			</label>

			<label>
				<input class="paiement" type="radio" name="paiement" id="mastercard" />
				<img class="paiement" src="img/mastercard.png" alt="mastercard"/>
			</label>

			<label>
				<input class="paiement" type="radio" name="paiement" id="paypal" >
				<img class="paiement" src="img/paypal.png" alt="paypal">
			</label>
		</div>
		<br>

		<label for="amortissement">Une durée d'amortissement :</label>
		<input type="date" name="amortissement" value="2018-06-01" min="2018-06-01" required>
		<br>

		<label for="annee_courante">L'année courante :</label>
		<input type="number" name="amortissement" value="2018" min="2018" max="2018" required>
		<br>

		<input type="submit" name="envoyer">
	</form>



	</body>
	<footer>
	</footer>
</html>
