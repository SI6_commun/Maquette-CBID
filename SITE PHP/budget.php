<!DOCTYPE HTML>
<meta charset="UTF-8">
<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css">
		<link rel="stylesheet" href="style_menu.css" type="text/css">
		<link rel="stylesheet" href="style_tab.css" type="text/css">
		<link rel="stylesheet" href="style_form.css" type="text/css">
		<link rel="stylesheet" href="knacss.css">
	</head>
	<body>
		<?php include_once('header.html')?>

	<div class="titre">
		Affichage des budgets
	</div>

	<div>
		<div class="titre2">
			Affichage des lignes budgétaires :
		</div>

		<a href="addLigne.php">Ajout d'une ligne budgétaire</a>
			<table style="width:90%; margin-left:25px;">
					<tr>
						<th>&nbsp; Année &nbsp;</th>
						<th>&nbsp; Niveau de compte &nbsp;</th>
						<th>&nbsp; N° de ligne &nbsp;</th>
						<th>&nbsp; Intitulé de ligne &nbsp;</th>
						<th>&nbsp; Somme allouée &nbsp;</th>
						<th>&nbsp; Responsable &nbsp;</th>
						<th>&nbsp; Modification &nbsp;</th>

					</tr>

					<br>
					<tr>
						<td> 1999 </td>
						<td> 12 </td>
						<td> 406 </td>
						<td> Budget Football </td>
						<td> 1500 </td>
						<td> M.Monts</td>
						<td><a href="budgetModifier.php">Modifier</a><br><a href="budgetSupprimer.php">Supprimer</a></td>

					</tr>
					<br>
					<tr>
						<td> 2001 </td>
						<td> 5 </td>
						<td> 102 </td>
						<td> Budget Choucroute </td>
						<td> 942 </td>
						<td> Mme Proie</td>
						<td><a href="">Modifier</a><br><a href="">Supprimer</a></td>
					</tr>
					<br>
					<tr>
						<td> 2008 </td>
						<td> 3 </td>
						<td> 15 </td>
						<td> Budget Marathon </td>
						<td> 1300 </td>
						<td> Melle Grand</td>
						<td><a href="">Modifier</a><br><a href="">Supprimer</a></td>


					</tr>
			</table>
			<a href="budgetAjouter.php">Ajout</a>
	</div>


	<div>
		<div class="titre2">
			Affichage des entrées budgétaires :
		</div>
		<table style="width:90%;">
						<tr>
							<th>&nbsp; Date &nbsp;</th>
							<th>&nbsp; Date de mise en service &nbsp;</th>
							<th>&nbsp; Statut de l'entrée budgétaire &nbsp;</th>
							<th>&nbsp; Intitulé &nbsp;</th>
							<th>&nbsp; Prestataire &nbsp;</th>
							<th>&nbsp; Commentaire &nbsp;</th>
							<th>&nbsp; N° de ligne budgétaire associé à l'entrée &nbsp;</th>
							<th>&nbsp; Montant de l'écriture &nbsp;</th>
							<th>&nbsp; Moyen de paiement &nbsp;</th>
							<th>&nbsp; Durée d'amortissement &nbsp;</th>
							<th>&nbsp; Valeur de l'année courante &nbsp;</th>
						</tr>
						<br>
						<tr>
							<td> 15/01/2015 </td>
							<td> 27/12/2014 </td>
							<td> En attente </td>
							<td> Miss PACA </td>
							<td> Maire </td>
							<td> En l'attente d'une réponse</td>
							<td> 155 </td>
							<td> 800 </td>
							<td> Visa </td>
							<td> 1 mois </td>
							<td> 2015 </td>
						</tr>
						<br>
						<tr>
							<td> 25/03/2012 </td>
							<td> 28/07/2013 </td>
							<td> Acquis </td>
							<td> Bingo </td>
							<td> Maire </td>
							<td> / </td>
							<td> 10 </td>
							<td> 652 </td>
							<td> Paypal</td>
							<td> 1 jour </td>
							<td> 2012 </td>
						</tr>
			</table>
		</div>


	<a href="addLigne.php"><img src="img/move-to-next.png"></a>
	<a href="addEntry.php" style="float:right;"><img src="img/move-to-prec.png"></a>





	</body>
	<footer>
	</footer>
</html>
