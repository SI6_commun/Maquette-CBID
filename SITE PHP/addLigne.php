<!DOCTYPE HTML>
<meta charset="UTF-8">
<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css">
		<link rel="stylesheet" href="style_menu.css" type="text/css">
		<link rel="stylesheet" href="style_form.css" type="text/css">
		<link rel="stylesheet" href="knacss.css">

	</head>
	<body>
		<?php include_once('header.html')?>

	<div class="titre">
		Ajout d'une ligne budégtaire
	</div>

	<form id="formulaire" action="budget.php" method="post">
		<legend>Veuillez saisir une ligne :</legend>

		<label for="year">Une année :</label>
		<input type="month" name="year" value="2000-01">

		<label for="niv_compte">Le niveau de compte :</label>
		<input type="number" name="niv_compte" value="1" max="9" required>

		<label for="num_ligne">Un numéro de ligne :</label>
		<input type="number" name="num_ligne" value="1" max="999" required>

		<label for="int_ligne">Un intitulé de ligne :</label>
		<input type="text" name="int_ligne" placeholder="intitulé" required>

		<label for="somme">Une somme allouée (€) :</label>
		<input type="number" name="somme" value="1" required>

		<label for="responsable">Un responsable :</label>
		<input type="text" name="responsable" placeholder="responsable" required>
		<br>

		<input type="submit" name="envoyer">
	</form>



	</body>
	<footer>
	</footer>
</html>
